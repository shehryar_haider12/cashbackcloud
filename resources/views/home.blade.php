@extends('layouts.app')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="profile_box">
                    <p class="profile_box_p_ra">Your Profile</p>
                    <p class="balance_hed">Balance</p>
                    <p class="amount_pro">$2.46</p>
                    <p class="pendind_with_draw">Pending withdrawal: $541.00</p>
                    <div class="seprater_profile_box">
                    </div>
                    <div class="row pd_top_for_detail">
                        <div class="col-md-8 ">
                            <p class="details_profile"> Cashback on Skrill account 1 day ago|2 days ago</p>
                        </div>
                        <div class="col-md-1"><img src="images/men.png"></div>
                        <div class="col-md-3">
                            <p class="remaining_amount">+ $0.24</p>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col-md-6">
                <div class="profile_box">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="profile_box_p_ra">% Your cashback rate</p>
                        </div>
                        <div class="col-md-6">
                            <p style="text-align: left;

                            letter-spacing: 0px;
                            color: #096DA7;
                            opacity: 1;">Boost your cashback rate</p>
                        </div>
                    </div>
                    <p class="balance_hed">Cashback rate</p>
                    <p class="amount_pro">80 %</p>

                    <div class="seprater_profile_box">
                    </div>
                    <div class="">
                        <p>Cashback rate from your level</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p style="color:#096DA7;">Diamond</p>
                        </div>
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-3">
                            <p>80% / 85%</p>
                        </div>
                    </div>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                            <span class="sr-only">70% Complete</span>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to reach the next level</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">506 / 100,000</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to hold this level next month</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">506 / 25,0000</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="points_Detail">Points to hold this level next month</p>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-4">
                            <p class="points_fig">506 / 25,0000</p>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <p class="points_Detail">You have collected 579,856 points so far. You need 10 million points to reach.<strong class="points_fig"> Cloud Club membership.</strong></p>
                        </div>


                    </div>



                </div>
                <div class="col-md-6">

                    <img class="banner_cover" src="images/ban.PNG" style="width: 100%;">

                </div>



            </div>
        </div>



    </div>

    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <img class="banner_cover" src="images/ban.PNG" style="width: 100%;">

            </div>
            <div class="col-md-6">
                <div class="profile_box">
                    <p class="profile_box_p_ra">Referral bonuses</p>
                    <p style="text-align: right;">Pending withdrawal: $541.00</p>
                    <div class="row">

                        <div class="col-md-6">
                            <p class="amount_pro">$2.46</p>
                        </div>
                        <div class="col-md-4 offset-md-2">
                            <p style=" lletter-spacing: 0px;
                            color: #B435AF;
                            opacity: 1;
                            font-size: 20px;
                            font-weight: 600;">0New</p>
                        </div>
                    </div>


                    <div class="seprater_profile_box">
                    </div>
                    <h6>Referred users</h6>
                    <div class="row">

                        <div class="col-md-3">Nawaz</div>
                        <div class="col-md-3">Nawaz</div>
                        <div class="col-md-3">Nawaz</div>
                        <div class="col-md-3">Nawaz</div>
                    </div>

                </div>
            </div>


        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="profile_box">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="profile_box_p_ra">$ Cashback Earning</p>
                        </div>
                        <div class="col-md-6">
                            <form action="#">
                                <div class="form-group">

                                    <select class="form-control" id="sel1" name="sellist1">
                                    <option>In this Month</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                  </select>
                            </form>
                            </div>
                        </div>

                        <img src="images/chart.svg" style="width: 100%;"></img>

                        <p class="profile_box_p_ra pd_top_extra">Detailed cashback calendar by accounts</p>
                        <table id="companies_detail_table" class="table">

                            <tbody>
                                <tr>
                                    <th scope="row"><img src="images/newcl.PNG" style="width: 120px;"></th>
                                    <td class="detail_and_company">Forex account, ID: 1097433</td>
                                    <td class="point_in_tbl">0 points</td>
                                    <td class="amount_in_tbl">$0.24</td>
                                </tr>
                                <tr>
                                    <th scope="row"><img src="images/newcl.PNG" style="width: 120px;"></th>
                                    <td class="detail_and_company">Forex account, ID: 1097433</td>
                                    <td class="point_in_tbl">0 points</td>
                                    <td class="amount_in_tbl">$0.24</td>
                                </tr>
                                <tr>
                                    <th scope="row"><img src="images/newcl.PNG" style="width: 120px;"></th>
                                    <td class="detail_and_company">Forex account, ID: 1097433</td>
                                    <td class="point_in_tbl">0 points</td>
                                    <td class="amount_in_tbl">$0.24</td>
                                </tr>
                                <tr>
                                    <th scope="row"><img src="images/newcl.PNG" style="width: 120px;"></th>
                                    <td class="detail_and_company">Forex account, ID: 1097433</td>
                                    <td class="point_in_tbl">0 points</td>
                                    <td class="amount_in_tbl">$0.24</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>
    </div>



</section>
@endsection
