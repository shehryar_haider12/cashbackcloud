<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Cash BAck</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">


        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="css/modern-business.css" rel="stylesheet">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    </head>


<body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="index.html"></a>
            <img class='logo_f' src="images/logo.png">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="about.html">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="how-forex-work.html">HOW FOREX WORKS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="feature.html">FEATURES</a>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="support.html">SUPPORT</a>

                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="contact.html">CONTACT</a>

                    </li>


                </ul>
                <ul>
                    @if (Route::has('login'))
                    <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                        @auth

                            {{-- <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a> --}}
                        @else
                            <li>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary btn_sign_modal_nav" data-toggle="modal" data-target="#exampleModal">
                        Sign up
                        </button></li>
                            <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                            @endif
                        @endauth
                    </div>
                @endif

                </ul>
            </div>
        </div>
    </nav>

    <section id="hero1" class="hero">

        <div class="inner">
            <div class="copy">
                <h1 class="banner_text">Earn With Every trade <br> You Make!</h1>


            </div>
        </div>
    </section>

    <section id="work_box_sec">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="how_work">
                        <div class="row">
                            <div class="col-md-1"><img class="work_icon" src="images/Icon.png"></div>
                            <div class="col-md-5">
                                <p class="work_title">How Forex Cash Back Works</p>
                            </div>
                        </div>
                        <p class="p_cls">When you open your Forex trading account (or connect an existing one) through us, your broker pays us a rebate for every trade. We then pay you back the majority of this rebate which you can withdraw at any time. Keep in mind that
                            your trading conditions (including spreads) remain exactly the same as if you had opened the account directly with the broker, so in effect, you’re reducing trading costs and improving profitability.</p>
                    </div>


                </div>
            </div>

    </section>
    <section id="feature_sec">
        <h1 class="heading_fea">FEATURES</h1>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="box_fea">
                        <h3 class="fea_box_hed">Reporting of Rebates</h3>
                        <p class="para_fea">Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit amet, consectetur dolor sit amet, consectetur</p>
                        <h6 class="get_start">GET STARTED</h6>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box_fea">
                        <h3 class="fea_box_hed">Reporting of Rebates</h3>
                        <p class="para_fea">Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit amet, consectetur dolor sit amet, consectetur</p>
                        <h6 class="get_start">GET STARTED</h6>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="box_fea">
                        <h3 class="fea_box_hed">Reporting of Rebates</h3>
                        <p class="para_fea">Lorem ipsum dolor sit amet, consectetur adipiscing elit dolor sit amet, consectetur dolor sit amet, consectetur</p>
                        <h6 class="get_start">GET STARTED</h6>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section id="broker_sec">
        <h1 class="heading_fea">BROKERS</h1>
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <div class="bg_broker"></div>
                </div>
                <div class="row mg_top">
                    <div class="col-md-3"><img class="broker_logo" src="images/fbs.png"></div>
                    <div class="col-md-3"><img class="broker_logo" src="images/fbs.png"></div>

                    <div class="col-md-3"><img class="broker_logo" src="images/fbs.png"></div>

                    <div class="col-md-3"><img class="broker_logo" src="images/fbs.png"></div>
                </div>

            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h6 class="broker_title_h">HotForex</h6>

                    <p class="broker_title_p">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                </div>

                <div class="col-md-3">
                    <h6 class="broker_title_h">HotForex</h6>

                    <p class="broker_title_p">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                </div>

                <div class="col-md-3">
                    <h6 class="broker_title_h">HotForex</h6>

                    <p class="broker_title_p">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                </div>


                <div class="col-md-3">
                    <h6 class="broker_title_h">HotForex</h6>

                    <p class="broker_title_p">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                </div>
            </div>
        </div>

    </section>



    <section id="sign-up">



        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <form method="POST" action="#" role="form">
                                            <div class="form-group">
                                                <h2>SIGN UP</h2>
                                            </div>
                                            <div class="form-group">
                                                <input id="signupName" type="text" maxlength="50" class="form-control" placeholder="Your name">
                                            </div>
                                            <div class="form-group">
                                                <input id="signupEmail" type="email" maxlength="50" class="form-control" placeholder="Email">
                                            </div>

                                            <div class="form-group">
                                                <input id="signupPassword" type="password" maxlength="25" class="form-control" placeholder="at least 6 characters" length="40">
                                            </div>

                                            <div class="form-group">
                                                <button id="signupSubmit" type="submit" class="btn btn-info btn-block">Create your account</button>
                                            </div>
                                            <p class="form-group">By creating an account, you agree to our <a href="#">Terms of Use</a> and our <a href="#">Privacy Policy</a>.</p>
                                            <hr>
                                            <p></p>Already have an account? <a href="#">Sign in</a></p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </section>


    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h5 class="hed_foter">How forex works</h5>
                    <p class="para_foter">He Printing And Typese tting Indus try. Lorem Ipsum has Been The Type</p>
                </div>

                <div class="col-md-2">
                    <h5 class="hed_foter"> Features</h5>
                    <p class="para_foter">Reporting of Rebates High Rebate Rates Highly Efficient custome</p>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-4">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <p class="para_foter">Reporting of Rebates High Rebate Rates Highly Efficient custome</p>
                </div>

                <div class="col-md-2">
                    <h5 class="hed_foter">Address Company</h5>
                    <p class="para_foter">Rottherdame Westerly Street, 678G Fax 20097 Swiss</p>
                </div>
                <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-4">
                            <p>Terms of Service</p>
                        </div>

                        <div class="col-md-5">
                            <ul class="social-network social-circle">
                                <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>


                            </ul>
                        </div>

                        <div class="col-md-3">
                            <p>Privacy Policy</p>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
