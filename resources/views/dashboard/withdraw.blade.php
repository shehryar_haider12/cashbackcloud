@extends('layouts.app')

@section('content')

<section id="withdrawl">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <p class="title">
                        You have pending withdrawals
                    </p>
                    <span class="title_span">You've requested for withdrawals below. Is your withdrawal late? Learn more about our withdrawal process here.</span>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="inner_shadow">
                                <p class="box_paragra"><strong> $520.00</strong> (fee: $18.20)<strong>SKRILL</strong> (omairlodhi@gmail.com) 2021.09.22 19:12:51 cancel withdrawal</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="inner_shadow">
                                <p class="box_paragra"><strong> $520.00</strong> (fee: $18.20)<strong>SKRILL</strong> (omairlodhi@gmail.com) 2021.09.22 19:12:51 cancel withdrawal</p>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>

@endsection
