@extends('layouts.app')

@section('content')

<section id="leader_board">
    <h2>Monthly price pool: $2,000</h2>
    <div class="container">

        <div class="row pd_top">

            <div class="col-md-9 offset-md-2">
                <div class="inner_shadow">
                    <div class="row pad-ext_row">
                        <div class="col-md-6"></div>
                        <div class="col-md-3">
                            <p class="Cbc_points">Cbc points in the month</p>
                        </div>
                        <div class="col-md-3">
                            <p class="Cbc_points">Reward in 2021/5</p>
                        </div>

                    </div>
                    <div class="row border_st">

                        <div class="col-md-6">

                            <p class="details"><img src="images/SURINAME.svg"> John******s</p>
                        </div>



                        <div class="col-md-3">
                            <p class="details">0</p>

                        </div>

                        <div class="col-md-3">
                            <p class="details">$500</p>
                        </div>

                    </div>


                    <!-- two row -->
                    <div class="row border_st">

                        <div class="col-md-6">

                            <p class="details"><img src="images/SURINAME.svg"> John******s</p>
                        </div>



                        <div class="col-md-3">
                            <p class="details">0</p>

                        </div>

                        <div class="col-md-3">
                            <p class="details">$500</p>
                        </div>

                    </div>

                    <!-- three -->
                    <div class="row border_st">

                        <div class="col-md-6">

                            <p class="details"><img src="images/SURINAME.svg"> John******s</p>
                        </div>



                        <div class="col-md-3">
                            <p class="details">0</p>

                        </div>

                        <div class="col-md-3">
                            <p class="details">$500</p>
                        </div>

                    </div>

                </div>
            </div>


        </div>
    </div>

</section>

@endsection
