<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/modern-business.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}" defer></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>

</head>
<body>

    <!-- Navigation -->
    <section id="dashborad_page_top_menu">
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="index.html"></a>
                <img class='logo_f' src="images/logo.png">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="about.html">>FOREX</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="services.html">>BINARY OPTION</a>
                        </li>


                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="services.html"><i class="fas fa-bell"></i></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            EN
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">

                                <a class="dropdown-item" href="pricing.html">Pricing Table</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                REFERAL
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                                <a class="dropdown-item" href="pricing.html">Pricing Table</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <p class="dynamic_per_perpage">80%</p>
                            </a>
                        </li>
                        @auth
                            <li class="nav-item">
                                <p class="dynamic_per_perpage">{{ Auth::user()->name }}</p>
                                </a>
                            </li>
                        @endauth

                    </ul>

                </div>
            </div>
        </nav>
    </section>
    <section id="nav_dashboard" style="margin-top: 67px;"></section>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav w-100 justify-content-center px-3">
                    <li class="nav-item {{ (request()->routeIs('home')) ? 'active' : '' }}">
                        <a class="nav-link btn {{ (request()->routeIs('home')) ? 'active' : '' }}" href="{{ route('home') }}">DASHBOARD</a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('my_account')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('my_account')) ? 'active' : '' }}" href="{{ route('my_account') }}">MY ACCOUNTS</a></a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('refered_user')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('refered_user')) ? 'active' : '' }}" href="{{ route('refered_user') }}">REFERRED USERS</a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('withdrawl')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('withdrawl')) ? 'active' : '' }}" href="{{ route('withdrawl') }}">WITHDRAWL</a>
                    </li>
                    <li class="nav-item {{ (request()->routeIs('leaderboard')) ? 'active' : '' }}">
                        <a class="nav-link {{ (request()->routeIs('leaderboard')) ? 'active' : '' }}" href="{{ route('leaderboard') }}">LEADERBOARD</a>
                    </li>

                </ul>
            </div>
        </nav>
    </section>

    {{-- <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div> --}}

    @yield('content');


    <footer class="section footer-classic context-dark bg-image bg_footer_gra">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h5 class="hed_foter">How forex works</h5>
                    <p class="para_foter">He Printing And Typese tting Indus try. Lorem Ipsum has Been The Type</p>
                </div>

                <div class="col-md-2">
                    <h5 class="hed_foter"> Features</h5>
                    <p class="para_foter">Reporting of Rebates High Rebate Rates Highly Efficient custome</p>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-4">
                    <h5 class="hed_foter">Join Our Newsletter</h5>
                    <p class="para_foter">Reporting of Rebates High Rebate Rates Highly Efficient custome</p>
                </div>

                <div class="col-md-2">
                    <h5 class="hed_foter">Address Company</h5>
                    <p class="para_foter">Rottherdame Westerly Street, 678G Fax 20097 Swiss</p>
                </div>
                <div class="container">
                    <div class="row pd">
                        <div class="seprater"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-4">
                            <p>Terms of Service</p>
                        </div>

                        <div class="col-md-5">
                            <ul class="social-network social-circle">
                                <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>


                            </ul>
                        </div>

                        <div class="col-md-3">
                            <p>Privacy Policy</p>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </footer>

</body>

</html>
