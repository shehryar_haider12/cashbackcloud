<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('wellcome');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->group(function () {
    Route::get('/myaccount', 'AccountController@index')->name('my_account');
    Route::get('/refered_user', 'ReferedUserController@index')->name('refered_user');
    Route::get('/withdrawl', 'withdrawlController@index')->name('withdrawl');
    Route::get('/leaderboard', 'LeaderboardController@index')->name('leaderboard');
});
